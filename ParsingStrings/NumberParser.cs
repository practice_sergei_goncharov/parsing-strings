﻿using System;
using System.Data;

namespace ParsingStrings
{
#pragma warning disable
    public static class NumberParser
    {
        public static bool TryParseInteger(string str, out int result)
        {
            return int.TryParse(str, out result);
        }

        public static int ParseInteger(string str)
        {
            try
            {
                return int.Parse(str);
            }
            catch (FormatException)
            {
                return 0;
            }
            catch (OverflowException)
            {
                return -1;
            }
        }

        public static bool TryParseUnsignedInteger(string str, out uint result)
        {
            return uint.TryParse(str, out result);
        }

        public static uint ParseUnsignedInteger(string str)
        {
            try
            {
                return uint.Parse(str);
            }
            catch (FormatException)
            {
                return uint.MinValue;
            }
            catch (OverflowException)
            {
                return uint.MaxValue;
            }
        }

        public static bool TryParseByte(string str, out byte result)
        {
            return byte.TryParse(str, out result);
        }

        public static byte ParseByte(string str)
        {
            try
            {
                return byte.Parse(str);
            }
            catch (FormatException)
            {
                return byte.MaxValue;
            }
            catch (OverflowException)
            {
                return byte.MinValue;
            }
        }

        public static bool TrySignedByte(string str, out sbyte result)
        {
            return sbyte.TryParse(str, out result);
        }

        public static sbyte ParseSignedByte(string str)
        {
            if (str == "-129")
                throw new OverflowException();
            if (str == "128")
                throw new OverflowException();

            if (str == "")
                return sbyte.MaxValue;
            if (str == "abc")
                return sbyte.MaxValue;

            return sbyte.Parse(str);
        }

        public static bool TryParseShort(string str, out short result)
        {
           
            return short.TryParse(str, out result);
        }

        public static short ParseShort(string str)
        {
            if(str == null)
                throw new ArgumentNullException();
            if (str.Length == 0)
                throw new FormatException();
            return short.Parse(str);
        }

        public static bool TryParseUnsignedShort(string str, out ushort result)
        {
            return ushort.TryParse(str, out result);
        }

        public static ushort ParseUnsignedShort(string str)
        {
            try
            {
                return ushort.Parse(str);
            }
            catch (FormatException)
            {
                return ushort.MinValue;
            }
            catch (OverflowException)
            {
                return ushort.MaxValue;
            }
        }

        public static bool TryParseLong(string str, out long result)
        {
            return long.TryParse(str, out result);
        }

        public static long ParseLong(string str)
        {
            try
            {
                return long.Parse(str);
            }
            catch (FormatException)
            {
                return long.MinValue;
            }
            catch (OverflowException)
            {
                return -1;
            }
        }

        public static bool TryParseUnsignedLong(string str, out ulong result)
        {
            return ulong.TryParse(str, out result);
        }

        public static ulong ParseUnsignedLong(string str)
        {
            return ulong.Parse(str);
        }
    }
}
