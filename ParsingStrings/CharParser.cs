﻿using System;

namespace ParsingStrings
{
    public static class CharParser
    {
#pragma warning disable
        public static bool TryParseChar(string str, out char result)
        {
            return char.TryParse(str, out result);
        }

        public static char ParseChar(string str)
        {
            try
            {
                return char.Parse(str);
            }
            catch (FormatException)
            {
                // Return a space character if a formatting error occurs
                return ' ';
            }
        }
    }
}
