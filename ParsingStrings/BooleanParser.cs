﻿using System;

namespace ParsingStrings
{
    public static class BooleanParser
    {
#pragma warning disable
        public static bool TryParseBoolean(string str, out bool result)
        {
            return bool.TryParse(str, out result);
        }

        public static bool ParseBoolean(string str)
        {
            if(str == null)
                throw new ArgumentNullException("Invalid boolean string representation.");
            if(str.Length == 0)
                return false;
            try
            {
                return bool.Parse(str);
            }
            catch (FormatException)
            {
                throw new ArgumentException("Invalid boolean string representation.");
            }
        }
    }
}
